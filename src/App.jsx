
import './App.css'
import Header from './components/Header'
import Die from './components/Die';
import { nanoid } from 'nanoid'
import { useEffect, useState } from 'react';


function App() {
  
  const [dice,setDice]= useState(newDice())
  const [win, setWin] = useState(false)
  


  function newDice(){
    let myarray = []
    for (let i = 0 ; i < 10 ; i++){
      let randomNum = Math.floor(Math.random() * 6) + 1;
      let diceitem = {
        id : nanoid(),
        value : randomNum,
        isheld: false
      }
      myarray.push(diceitem)
    }
    return myarray
  }

  function handleClick(){
    let updateArray = []
    let newArray = newDice()
    for (let i = 0 ; i < 10 ; i++){
      if (!dice[0]){
        setDice(newDice())
        return
      }
      else {
        updateArray[i] = dice[i].isheld ? dice[i] : newArray[i]
      }
    }
      setDice(updateArray)
  }

  function dieClick(id){ 
    const newArray = dice.map(item => item.id ===id ? {...item , isheld:!item.isheld } : item)
    setDice(newArray)
    }    
    

  
    useEffect(()=>{
    let firstvalue = 2
    if (dice.every(item => (item.isheld && item.value === firstvalue))){
      console.log("you won !!!")
      setWin(true)
     }
    },[dice])
  
  console.log(win)
  const element = dice.map(item => <Die  key={item.id}
  number={item.value} Click={() => dieClick(item.id)}  isChosen={item.isheld}/>)

  return (
    <div className="App">
      <Header />
      <div className='die-block'>{element}</div>
      <br />
      <button className='btn' onClick={handleClick}>{win ? "Play again" : "Roll"  }</button>
    </div>
  )
}

export default App
