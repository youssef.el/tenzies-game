import React from 'react'

const Die = (props) => {

  return (
    <button className='die' 
    style={{
        backgroundColor: props.isChosen? "skyblue":"white"
    }}
     onClick={props.Click}>{props.number}</button>
  )
}

export default Die
