import React from 'react'

const Header = () => {
  return (
    <div>
      <h2>Tenzies</h2>
      <p>Roll until the dice are the same. Click each die to freeze it at its current value between rolls.</p>
    </div>
  )
}

export default Header
